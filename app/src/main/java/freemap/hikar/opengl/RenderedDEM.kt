/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.opengl

import java.nio.ByteBuffer
import java.nio.ByteOrder
import freemap.data.Point
import android.opengl.GLES20
import freemap.jdem.DEM
import java.nio.FloatBuffer
import java.nio.ShortBuffer


class RenderedDEM(dem: DEM) {

    private var vertexBuffer: FloatBuffer? = null
    private var indexBuffer: ShortBuffer? = null
    private val surfaceColour = floatArrayOf(0.0f, 1.0f, 0.0f, 0.0f)

    private val centrePoint = Point()


    init {
        val nrows = dem.ptHeight
        val ncols = dem.ptWidth
        val nvertices = nrows * ncols

        val buf = ByteBuffer.allocateDirect(nvertices * 3 * 4)
        buf.order(ByteOrder.nativeOrder())
        vertexBuffer = buf.asFloatBuffer()

        // triangle strip with degenerate triangles to allow multiple lines
        val nIndices = (ncols * 2 + 2) * (nrows - 1) - 2

        val ibuf = ByteBuffer.allocateDirect(nIndices * 2)
        ibuf.order(ByteOrder.nativeOrder())
        indexBuffer = ibuf.asShortBuffer()

        // Point bottomLeft = trans.tileToDisplay(dem.getBottomLeft()), topRight = trans.tileToDisplay(dem.getTopRight());
        // TODO fix this replace TileProjectionDisplayTransformation
        val bottomLeft = dem.bottomLeft
        val topRight = dem.topRight
        centrePoint.x = (bottomLeft.x + topRight.x) / 2
        centrePoint.y = (bottomLeft.y + topRight.y) / 2


        for (row in 0 until nrows) {
            for (col in 0 until ncols) {
                // TODO fix this replace TileProjectionDisplayTransformation
                val p = dem.getPoint(col, row) // trans.tileToDisplay(dem.getPoint(col, row));

                vertexBuffer?.put(p.x.toFloat())
                vertexBuffer?.put(p.y.toFloat())
                vertexBuffer?.put((p.z - 5).toFloat())
            }
        }

        vertexBuffer?.position(0)

        val indices = ShortArray(nIndices)


        var i = 0
        for (row in 0 until nrows - 1) {
            for (col in 0 until ncols) {
                indices[i++] = (row * ncols + col).toShort()
                indices[i++] = (indices[i - 2] + ncols).toShort()
            }


            // degenerate triangles
            if (row < nrows - 2) {
                indices[i++] = indices[i - 2]
                indices[i++] = (indices[i - 2] - ncols + 1).toShort()
            }
        }

        //Log.d("hikar","indices (first 3 rows and last row): " + istr);


        indexBuffer?.put(indices)
        indexBuffer?.position(0)
    }


    fun render(gpu: GPUInterface) {
        gpu.setUniform4fv("uColour", surfaceColour)
        gpu.drawBufferedData(vertexBuffer!!, indexBuffer!!, 12, "aVertex", GLES20.GL_TRIANGLE_STRIP)
    }

    fun centreDistanceTo(p: Point): Double {
        return centrePoint.distanceTo(p)
    }
}
