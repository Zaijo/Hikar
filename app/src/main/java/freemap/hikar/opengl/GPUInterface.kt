package freemap.hikar.opengl

import android.opengl.GLES20
import android.util.Log

import java.nio.Buffer


// Controls the interface between CPU and GPU, i.e. all the interfacing with shaders
// and associating buffer data with shader variables.
/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

open class GPUInterface(val id: String, vertexShaderCode: String, fragmentShaderCode: String) {

    private var shaderProgram = -1


    init {
        val vertexShader = addVertexShader(vertexShaderCode)

        if (vertexShader >= 0) {
            val fragmentShader = addFragmentShader(fragmentShaderCode)
            if (fragmentShader >= 0) {
                shaderProgram = makeProgram(vertexShader, fragmentShader)
            }
        }
    }

    private fun addVertexShader(shaderCode: String): Int {
        return getShader(GLES20.GL_VERTEX_SHADER, shaderCode)
    }

    private fun addFragmentShader(shaderCode: String): Int {
        return getShader(GLES20.GL_FRAGMENT_SHADER, shaderCode)
    }

    private fun getShader(shaderType: Int, shaderCode: String): Int {
        val shader = GLES20.glCreateShader(shaderType)
        GLES20.glShaderSource(shader, shaderCode)
        GLES20.glCompileShader(shader)
        val compileStatus = IntArray(1)
        GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compileStatus, 0)
      //  Log.d("hikar", "Compile status for wayId: " + id + "=" + compileStatus[0]);
        if (compileStatus[0] == 0) {
         //   Log.e("hikar", "Error compiling shader: " + GLES20.glGetShaderInfoLog(shader));
            GLES20.glDeleteShader(shader)
            return -1
        }
        return shader
    }

    private fun makeProgram(vertexShader: Int, fragmentShader: Int) : Int {
        val shaderProgram = GLES20.glCreateProgram()
        GLES20.glAttachShader(shaderProgram, vertexShader)
        GLES20.glAttachShader(shaderProgram, fragmentShader)
        GLES20.glLinkProgram(shaderProgram)
        val linkStatus = IntArray(1)
        GLES20.glGetProgramiv(shaderProgram, GLES20.GL_LINK_STATUS, linkStatus, 0)
   //     Log.d("hikar", "makeProgram status for wayId: " + id + "=" + linkStatus[0]);
        if (linkStatus[0] == 0) {
     //       Log.e("hikar", "Error linking shader program: " + GLES20.glGetProgramInfoLog(shaderProgram));
            GLES20.glDeleteProgram(shaderProgram)
            return -1
        }
        GLES20.glUseProgram(shaderProgram)
        return shaderProgram
    }

    // Draw buffered data:
    // we select the buffer, get the shader var ref, tell opengl the format of the data
    // and then draw the data
    fun drawBufferedData(vertices: Buffer, indices: Buffer, stride: Int, attrVar: String) {
        drawBufferedData(vertices, indices, stride, attrVar, GLES20.GL_TRIANGLES)
    }

    fun drawBufferedData(vertices: Buffer, indices: Buffer,  stride: Int, attrVar: String, mode: Int) {
        if (isValid()) {
            val attrVarRef = getShaderVarRef(attrVar)
            vertices.position(0)
            indices.position(0)
            GLES20.glEnableVertexAttribArray(attrVarRef)
            GLES20.glVertexAttribPointer(attrVarRef, 3, GLES20.GL_FLOAT, false, stride, vertices)
            GLES20.glDrawElements(mode, indices.limit(), GLES20.GL_UNSIGNED_SHORT, indices)
        }
    }

    private fun getShaderVarRef(shaderVar: String): Int {

        return if (isValid()) GLES20.glGetAttribLocation(shaderProgram, shaderVar) else -1
    }


    // Do something with the modelview and perspective matrices

    fun sendMatrix(mtx: FloatArray, shaderMtxVar: String) {
        if (isValid()) {
            val refMtxVar = GLES20.glGetUniformLocation(shaderProgram, shaderMtxVar)
            //    Log.d("hikar", "for GPUInterface " + wayId + " and shaderProgram " + shaderProgram + ": sendMatrix(): refMtxVar=" + refMtxVar + " for shader variable: " + shaderMtxVar);
            errorCheck("glGetUniformLocation")
            GLES20.glUniformMatrix4fv(refMtxVar, 1, false, mtx, 0) // 1 = one matrix http://www.khronos.org/opengles/sdk/docs/man/xhtml/glUniform.xml
            errorCheck("sending over matrix")
        }
    }

    // could be used e.g. for sending colours
    fun setUniform4fv(shaderVar: String, value: FloatArray) {
        if (isValid()) {
            val refShaderVar = GLES20.glGetUniformLocation(shaderProgram, shaderVar)
            GLES20.glUniform4fv(refShaderVar, 1, value, 0) // 1 = one uniform variable http://www.khronos.org/opengles/sdk/docs/man/xhtml/glUniform.xml
        }
    }

    // could be used e.g. for sending texture wayId
    fun setUniform1i(shaderVar: String, i: Int) {
        if (isValid()) {
            val refShaderVar = GLES20.glGetUniformLocation(shaderProgram, shaderVar)
            GLES20.glUniform1i(refShaderVar, i)
        }
    }

    fun select() {
        GLES20.glUseProgram(shaderProgram)
    }

    private fun isValid(): Boolean {
        return shaderProgram >= 0
    }

    fun drawTexturedBufferedData(vertices: Buffer, indices: Buffer, attrVar: String, attrTexVar: String, mode: Int) {
        if (isValid()) {


            val attrVarRef = getShaderVarRef(attrVar)
            errorCheck("getShaderVarRef, vertices, attrVar= $attrVar attrVarRef=$attrVarRef")
            GLES20.glEnableVertexAttribArray(attrVarRef)
            errorCheck("glEnableVertexAttribArray, vertices")


            vertices.position(0)
            GLES20.glVertexAttribPointer(attrVarRef, 3, GLES20.GL_FLOAT, false, 20, vertices)
            errorCheck("glVertexAttribPointer, vertices")

            val attrTexVarRef = getShaderVarRef(attrTexVar)
            errorCheck("getShaderVarRef, tex")
            GLES20.glEnableVertexAttribArray(attrTexVarRef)
            errorCheck("glEnableVertexAttribArray, tex")
            vertices.position(3)
            GLES20.glVertexAttribPointer(attrTexVarRef, 2, GLES20.GL_FLOAT, false, 20, vertices)
            errorCheck("glVertexAttribPointer, tex")

            indices.position(0)
            //   vertices.position(0);
            GLES20.glDrawElements(mode, indices.limit(), GLES20.GL_UNSIGNED_SHORT, indices)
            errorCheck("glDrawElements")

        }
    }

    private fun errorCheck(location: String) {
        val i = GLES20.glGetError()
        if(i != GLES20.GL_NO_ERROR) {
            Log.e("hikar", "**********OpenGL error for GPU interface $id at $location code $i")
        }
    }
}

