/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar

import android.os.Bundle
import android.text.InputType
import androidx.preference.PreferenceFragmentCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.EditTextPreference


class Preferences: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        supportFragmentManager.beginTransaction().replace(android.R.id.content, PrefFragment()).commit()
    }
}

class PrefFragment: PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)
        val editTextPreference: EditTextPreference? = findPreference("password") as EditTextPreference?
        editTextPreference?.setOnBindEditTextListener {
            it.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        }
    }
}